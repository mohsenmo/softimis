import React from "react";

const Loader = () => {
	return (
		<div id="ht-preloader">
			<div className="loader clear-loader">
				<div className="loader-box"></div>
				<div className="loader-box"></div>
				<div className="loader-box"></div>
				<div className="loader-box"></div>
				<div className="loader-wrap-text">
					<div className="text">
						<span>S</span>
						<span>O</span>
						<span>F</span>
						<span>T</span>
						<span>I</span>
						<span>M</span>
						<span>I</span>
						<span>S</span>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Loader;
