import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import HomeOne from "./home/homeOne";
import HomeTwo from "./home/homeTwo";
import HomeThree from "./home/homeThree";
import HomeFour from "./home/homeFour";
import HomeFive from "./home/homeFive";
import HomeSix from "./home/homeSix";
import Team from "./pages/team";
import TeamSingle from "./pages/teamSingle";
import Faq from "./pages/faq";
import Error404 from "./pages/error404";
import ComingSoon from "./pages/comingSoon";
import Accordion from "./pages/element/accordion";
import BlogPost from "./pages/element/blogPost";
import Counter from "./pages/element/counter";
import FeatureBox from "./pages/element/featureBox";
import PricingTable from "./pages/element/pricingTable";
import TeamElement from "./pages/element/team";
import Testimonial from "./pages/element/testimonial";
import AboutOne from "./about/aboutOne";
import AboutTwo from "./about/aboutTwo";
import ServiceOne from "./services/serviceOne";
import ServiceTwo from "./services/serviceTwo";
import ContactOne from "./contact/contactOne";
import ContactTwo from "./contact/contactTwo";
import BlogGrid from "./blogs/blogGrid";
import LeftSidebar from "./blogs/leftSidebar";
import RightSidebar from "./blogs/rightSidebar";
import BlogSingle from "./blogs/blogSingle";
import Login from "./login";
import SignUp from "./signup";
import Privacy from "./policy/privacy";
import Terms from "./policy/terms";
import LoginSuccess from "./pages/loginSuccess";
import SignupSuccess from "./pages/signupSuccess";

function MainNavigation() {
  return (
    <Router>
      <Switch>
        {/** Home */}
        <Route exact path="/"  component={HomeFive} />
        <Route path="/home-one"  component={HomeOne} />
        <Route path="/home-two"  component={HomeTwo} />
        <Route path="/home-three"  component={HomeThree} />
        <Route path="/home-four"  component={HomeFour} />
        <Route path="/home-five"  component={HomeFive} />
        <Route path="/home-six"  component={HomeSix} />
        {/** Pages */}
        <Route path="/team"  component={Team} />
        <Route path="/teamsingle"  component={TeamSingle} />
        <Route path="/faq"  component={Faq} />
        <Route path="/comingsoon"  component={ComingSoon} />
        <Route path="/loginsuccess"  component={LoginSuccess} />
        <Route path="/signupsuccess"  component={SignupSuccess} />
        <Route path="/error404"  component={Error404} />
        {/** Elements */}
        <Route path="/accordion"  component={Accordion} />
        <Route path="/blogpost"  component={BlogPost} />
        <Route path="/counter"  component={Counter} />
        <Route path="/featurebox"  component={FeatureBox} />
        <Route path="/pricing"  component={PricingTable} />
        <Route path="/teamelement"  component={TeamElement} />
        <Route path="/testimonial"  component={Testimonial} />
        {/** About Services & Contact */}
        <Route path="/about"  component={AboutOne} />
        <Route path="/about-two"  component={AboutTwo} />
        <Route path="/service"  component={ServiceOne} />
        <Route path="/service-two"  component={ServiceTwo} />
        <Route path="/contact"  component={ContactOne} />
        <Route path="/contact-two"  component={ContactTwo} />
        {/** Blogs */}
        <Route path="/bloggrid"  component={BlogGrid} />
        <Route path="/leftsidebar"  component={LeftSidebar} />
        <Route path="/rightsidebar"  component={RightSidebar} />
        <Route path="/blogsingle"  component={BlogSingle} />
        {/** Login & Policy */}
        <Route path="/login"  component={Login} />
        <Route path="/register"  component={SignUp} />
        <Route path="/privacy"  component={Privacy} />
        <Route path="/terms"  component={Terms} />
        {/** invalid routes redirection */}
        <Route component={Error404} />
      </Switch>
    </Router>
  );
}

export default MainNavigation;
