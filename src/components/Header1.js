import React, { useState } from "react";
import Loader from "./Loader";
import dataHeader from "./data.json";
//import './theme-color/color-1.css'
const URL = require("./constants");

const Header1 = () => {
	console.log(dataHeader.data.home[0]);
	const storedPage = localStorage.getItem("page");
	const [breadcrumb] = useState(storedPage);
	const nowpage = window.location.pathname;
	const fun = (value) => {
		localStorage.setItem("page", value);
	};
	return (
		<>
			<Loader />
			<header id="site-header" className="header header-1">
				<div className="container-fluid">
					<div id="header-wrap" className="box-shadow">
						<div className="row">
							<div className="col-lg-12">
								{/* Navbar */}
								<nav className="navbar navbar-expand-lg">
									<a className="navbar-brand logo" href="/">
										<img
											id="logo-img"
											className="img-center"
											src={`${URL.img_url}${dataHeader.data.home[0].logo}`}
											alt=""
										/>
									</a>
									<button
										className="navbar-toggler"
										type="button"
										data-toggle="collapse"
										data-target="#navbarNavDropdown"
										aria-expanded="false"
										aria-label="Toggle navigation"
									>
										{" "}
										<span></span>
										<span></span>
										<span></span>
									</button>
									<div
										className="collapse navbar-collapse"
										id="navbarNavDropdown"
									>
										{/* Left nav */}
										<ul
											id="main-menu"
											className="nav navbar-nav ml-auto mr-auto"
										>
											<li className="nav-item">
												{" "}
												<a
													className={`nav-link ${
														nowpage === "/"
															? "active"
															: ""
													}`}
													href="/"
												>
													Home
												</a>

											</li>

											<li className="nav-item">
												{" "}
												<a
													className={`nav-link ${
														nowpage === "/service"
															? "active"
															: ""
													}`}
													href="/service"
												>
													Services
												</a>
											</li>
											<li className="nav-item">
												<a
													className={`nav-link ${
														nowpage === "/about"
															? "active"
															: ""
													}`}
													href="/about"
												>
													About Us
												</a>
											</li>

											<li className="nav-item">
												{" "}
												<a
													className={`nav-link ${
														nowpage === "/contact"
															? "active"
															: ""
													}`}
													href="/contact"
												>
													Contact Us
												</a>
											</li>
										</ul>
									</div>
									{/*
									<a
										className="btn btn-theme btn-sm"
										href={`/${dataHeader.data.login.link}`}
										data-text="Login"
									>
										{" "}
										<span>L</span>
										<span>o</span>
										<span>g</span>
										<span>i</span>
										<span>n</span>
									</a>
									*/}
								</nav>
							</div>
						</div>
					</div>
				</div>
			</header>
		</>
	);
};

export default Header1;
